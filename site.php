<!doctype html>

<html lang="ru">

  <head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="style.css">
    <title>WEB</title>
  </head>

  <body class="p-0 m-0 body" >
  
    <header class="container-fluid justify-content-center logo header">
      <div class="container col-md-8"><img id="logo" src="image.png" alt="logo"><span>Название сайта</span></div>
    </header>
    </div>
	<div class="row mx-auto">
      <nav class="col-md-8 pb-1 pt-1 mx-auto">
        <div class="row justify-content-center text-center logo" id="menu">
          <a class="col-12 col-md-4" href="#hyper">Гиперссылки</a>
          <a class="col-12 col-md-4" href="#table">Таблица</a>
          <a class="col-12 col-md-4" href="#forms">Форма</a>
        </div>
      </nav>
      
      <article class="col-md-8 mx-auto artic">
        <h1 class="text-center">WEB</h1>
        <div class="row">
          <section class="col-12 col-md-12 mb-5" id="forms">
            <h2>Формы</h2>
<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $mess) {
	print($mess);
  }
  print('</div><br/><br/>');
}
?>
            <form action="" method="POST">
              <br/>Имя:<br/>
              <input type="text" name="name" <?php if ($errors['name']) {print 'class="error"';} ?> value="<?php print $values['name']; ?>"><br/>
              <br/>email:<br/>
              <input type="email" name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>"><br/>
              <br/>Дата рождения:<br/>
                <input name="date" <?php if ($errors['date']) {print 'class="error"';} ?> value="<?php print $values['date']; ?>" type="text"/>
              <br/>
                <br/><a id="gender"></a>Пол:<br/>
                  <input type="radio" name="gender" value="male" <?php if ($values['gender'] == 'male') {print 'checked="checked"';} ?>/>Мужской<br/>
                  <input type="radio" name="gender" value="female" <?php if ($values['gender'] == 'female') {print 'checked="checked"';} ?>/>Женский<br/>
              <br/>Кол-во конечностей:<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value="1" <?php if ($values['limb'] == '1') {print 'checked="checked"';} ?> />1<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value="2" <?php if ($values['limb'] == '2') {print 'checked="checked"';} ?> />2<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value="3" <?php if ($values['limb'] == '3') {print 'checked="checked"';} ?> />3<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value="4" <?php if ($values['limb'] == '4') {print 'checked="checked"';} ?> />4<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value=">4" <?php if ($values['limb'] == '>4') {print 'checked="checked"';} ?> />>4<br/>    
              <br/>Сверхспособности:<br/>
                <input type="checkbox" name="super1" value="бессмертие" <?php if ($values['super1'] != '') {print 'checked="checked"';} ?> />Бессмертие<br/>
                <input type="checkbox" name="super2" value="прохождение сквозь стены" <?php if ($values['super2'] != '') {print 'checked="checked"';} ?> />Прохождение сквозь стены<br/>
                <input type="checkbox" name="super3" value="левитация" <?php if ($values['super3'] != '') {print 'checked="checked"';} ?> />Левитация<br/>
                <input type="checkbox" name="super4" value="зачёт по дискре" <?php if ($values['super4'] != '') {print 'checked="checked"';} ?> />Зачёт по дискретке за один день<br/>
              <br/>Биография:<br/>
                <textarea name="message" value="<?php print $values['message']; ?>">Расскажите о себе</textarea>
                <input type="checkbox" name="check" value="+" <?php if ($values['check'] != '') {print 'checked="checked"';} ?>/>С контрактом ознакомлен<br/>
              <input type="submit" name="send" value="Отправить" class="submit"/>        
            </form>
          </section>
	      <section class="col-12 col-md-12" id="hyper">
            <h2>Гиперссылки</h2>
            <ol>
              <li>Абсолютная <a href="http://htmlbook.ru/">гиперссылка</a> на главную страницу сайта example.com</li>
              <li>Абсолютная <a href="https://discordapp.com/">ссылка</a> на главную сайта example.com в протоколе https</li>
              <li><a href="ftp://ftp.intel.com/images/UserTroubleshootingPic1.JPG">Ссылка</a> на файл на сервере FTP без авторизации</li>
              <li><a href="ftp://u88187:TSzACa50G7ft@ftp88187.hostfx.ru/logs/ftp88187.hostfx.ru.access.log">Ссылка</a> на файл на сервере FTP с авторизацией</li>
              <li><a href="https://en.wikipedia.org/wiki/Ivan_III_of_Russia#Legacy">Ссылка</a> на фрагмент страницы некоторого сайта</li>
              <li><a href="#gender">Ссылка</a> на фрагмент текущей страницы</li>
              <li><a href="https://www.google.ru/search?client=opera&q=parameters&sourceid=opera&ie=UTF-8&oe=UTF-8">Параметры</a> - ссылка на сайт с параметрами в URL</li>
              <li><ol>
                <li><a href="#hyper">Гиперссылки</a></li>
                <li><a href="#forms">Форма</a></li>
              </ol></li>
              <li><a id="nonhref">Ссылка </a> без href</li>
              <li><a href="">Ссылка</a> с пустым href</li>
              <li><a href="https://ru.wikipedia.org/wiki/Экзаменационная_сессия" rel="nofollow">Ссылка</a>, по которой запрещен переход поисковикам</li>
              <li><a href="https://ru.wikipedia.org/wiki/Диплом" rel="nofollow">Ссылка</a>, которая абсолютно не индексируется</li>
              <li>Первой гражданской работой <a href="https://ru.wikipedia.org/wiki/Толкин,_Джон_Рональд_Руэл">Толкина</a> после Первой мировой войны стала должность помощника лексикографа в 1919 году</li>
              <li><a href="https://ru.wikipedia.org/wiki/Windows" target="_blank"><img src="https://lumpics.ru/wp-content/uploads/2018/02/kak-izmenit-knopku-Pusk-v-Windows-7.png" width="256" height="256" alt="windows"></a></li>
              <li><img src='http://wow.blizzwiki.ru/images/a/af/WorldMap-StormwindCity.jpg' width="772" height="515" alt="Stormwind" usemap="#stormwindmap">
                <map name="stormwindmap">
                  <area shape="circle" coords="485,365,50" href='http://mmoboom.ru/media/images/aa6815e3beb681a0.jpg' target="_blank" alt="Trade District">
                  <area shape="circle" coords="503,194,50" href='http://mmoboom.ru/media/images/b08d6402f6ed4ab3.jpg' target="_blank" alt="Dwarven District">
                  <area shape="circle" coords="569,304,50" href='http://mmoboom.ru/media/images/db92ea8702baae0d.jpg' target="_blank" alt="Old Town">
                  <area shape="circle" coords="414,270,50" href='http://mmoboom.ru/media/images/db1d34c5c21ae351.jpg' target="_blank" alt="Cathedral Square">
                  <area shape="circle" coords="375,419,60" href='http://s019.radikal.ru/i602/1207/43/f4590fb89c0e.jpg' target="_blank" alt="Mage Quarter">
                  <area shape="poly" coords="565,195,653,108,705,175,595,260" href='http://ksidden.net/wp-content/uploads/WoWScrnShot_030318_114157.jpg' target="_blank" alt="Stormwind Keep">
                  <area shape="poly" coords="510,430,542,397,597,457,550,487" href='https://mmo-obzor.ru/_bd/2/76996415.jpg' target="_blank" alt="Valley of Heroes">
                  <area shape="rect" coords="140,105,220,290" href='https://wowjp.net/_ph/1/281860116.jpg?1565482020' target="_blank" alt="Stormwind Harbor">
                </map></li>
              <li><a href="site.html">Ссылка</a> на файл в текущем каталоге</li>
              <li><a href="../about/site.html">Ссылка</a> на файл в каталоге about</li>
              <li><a href="../site.html">Ссылка</a> на файл в каталоге уровнем выше</li>
              <li><a href="../../site.html">Ссылка</a> на файл в каталоге двумя уровнями выше</li>
              <li>Сокращенная <a href="/">ссылка</a> на главную</li>
              <li>Сокращенная <a href="firstWEB.html">ссылка</a> на внутренюю</li>
            </ol>
          </section>
          <section class="col-12 col-md-12" id="table">
	        <h2>Таблица</h2>
	        <table class="table table-hover">
              <tr>
                <th>Первая колонка</th>
	      	  <th>Вторая колонка</th>
                <th>Третья колонка</th>
              </tr>
              <tr>
                <th>Первая колонка</th>
                <th>Вторая колонка</th>
                <th>Третья колонка</th>
              </tr>
              <tr>
                <th>Первая колонка</th>
                <th>Вторая колонка</th>
                <th>Третья колонка</th>
              </tr>
              <tr>
                <th>Первая колонка</th>
                <th>Вторая колонка</th>
                <th>Третья колонка</th>
              </tr>
              <tr>
                <th colspan="2">Первая+Вторая колонки</th>
                <th>Третья колонка</th>
              </tr>
              <tr>
                <th colspan="3">Первая+Вторая+Третья колонки</th>
              </tr>
            </table>
          </section>
        </div>
	  </article>
	</div>
    <footer class="container-fluid mt-1 justify-content-center footer">
        <div>(с) Мацока Григорий 2020</div>
    </footer>
  </body>
</html>
